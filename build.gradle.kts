import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.21"
    application
}

group = "edu.kotlin.homework"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("org.apache.pdfbox:pdfbox:2.0.22")
    implementation("com.github.javafaker:javafaker:1.0.2")
    testImplementation("junit:junit:4.13.2")
    testImplementation(("org.assertj:assertj-core:3.19.0"))
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("MainKt")
}