abstract class Pet

// Интерфейсы для возможности бегать и плавать
interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

// Класс Cat, реализующий интерфейсы Runnable и Swimmable
open class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and I am running")
    }

    override fun swim() {
        println("I am a Cat, and I am swimming")
    }
}

// Класс Fish, реализующий интерфейс Swimmable
open class Fish : Pet(), Swimmable {
    override fun swim() {
        println("I am a Fish, and I am swimming")
    }
}


class Tiger : Cat() {
    override fun run() {
        println("I am a Tiger, and I am running")
    }

    override fun swim() {
        println("I am a Tiger, and I am swimming")
    }
}

class Lion : Cat() {
    override fun run() {
        println("I am a Lion, and I am running")
    }
}

class Salmon : Fish() {
    override fun swim() {
        println("I am a Salmon, and I am swimming")
    }
}

// Обобщенные функции
fun <T> useSwimAndRunSkill(obj: T) where T: Runnable, T: Swimmable {
    obj.run()
    obj.swim()
}

fun <T: Runnable> useRunSkill(obj: T) {
    obj.run()
}

fun <T: Swimmable> useSwimSkill(obj: T) {
    obj.swim()
}

fun main() {
    val tiger = Tiger()
    val lion = Lion()
    val salmon = Salmon()

    useSwimAndRunSkill(tiger)
    useRunSkill(lion)
    useSwimSkill(salmon)
}
