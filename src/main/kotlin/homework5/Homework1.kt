package homework5

fun main() {
    val lion = Lion("Лев", 80, 68.8)
    lion.eat("Banana")
    val tiger = Tiger("Тигр", 70, 57.5)
    tiger.eat("Mango")
    val hippo = Hippo("Бегемот", 170, 100.9)
    hippo.eat("Apple")
    val wolf = Wolf("Волк", 60, 34.8)
    wolf.eat("Apple")
    val giraffe = Giraffe("Жираф", 250, 77.1)
    giraffe.eat("Meat")
    val elephant = Elephant("Слон", 200, 96.3)
    elephant.eat("Potato")
    val chimp = Chimp("Шимпанзе", 70, 25.5)
    chimp.eat("Tomato")
    val gorilla = Gorilla("Горилла", 100, 81.1)
    gorilla.eat("Cabbage")
    println("${lion.satiety} ${tiger.satiety} ${hippo.satiety} ${wolf.satiety} ${giraffe.satiety} ${elephant.satiety} ${chimp.satiety} ${gorilla.satiety}")
}

class Lion(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Apple")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Tiger(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Hippo(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Wolf(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Giraffe(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Elephant(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Chimp(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Gorilla(val name: String, var height: Int, var weight: Double) {
    private val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
    var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}