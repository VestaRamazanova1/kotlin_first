package homework5

fun main() {
    println("Введите целое число ")
    val number = readLine()!!.toInt()
    println(reverseNumber(number))
}

fun reverseNumber(value: Int): Int {
    var reversed = 0
    var number = value
    while (number != 0) {
        val digit = number % 10
        reversed = reversed * 10 + digit
        number /= 10
    }
    return reversed
}