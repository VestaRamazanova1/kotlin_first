import java.time.LocalDate
class Handler {
    // пустой класс для задания
}

fun typeCasting(obj: Any?) {
    when (obj) {
        is String -> println("Я получил String = '$obj', ее длина равна ${obj.length}")
        is Int -> println("Я получил Int = $obj, его квадрат равен ${obj * obj}")
        is Double -> {
            val roundedValue = "%.2f".format(obj)
            println("Я получил Double = $obj, это число округляется до $roundedValue")
        }
        is LocalDate -> {
            val tinkoffFoundedDate = LocalDate.of(2006, 12, 24)
            val comparisonResult = if (obj.isBefore(tinkoffFoundedDate)) "меньше" else "не меньше"
            println("Я получил LocalDate = $obj, она $comparisonResult даты основания Tinkoff")
        }
        null -> println("Объект равен null")
        else -> println("Мне этот тип неизвестен(")
    }
}
fun main() {
    val str = "Privet"
    val intNum = 145
    val doubleNum = 145.0
    val preciseDouble = 145.2817812
    val localDate = LocalDate.of(1990, 1, 1)
    val handlerClass = Handler::class

    typeCasting(str)
    typeCasting(intNum)
    typeCasting(doubleNum)
    typeCasting(preciseDouble)
    typeCasting(localDate)
    typeCasting(handlerClass)
}