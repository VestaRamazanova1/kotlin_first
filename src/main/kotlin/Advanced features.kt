fun configurPerson(
    name: String,
    surname: String,
    gender: String,
    dateOfBirth: String,
    age: Int,
    patronymic: String? = null,
    inn: String? = null,
    snils: String? = null,
) {
    println(
        "name = $name, " +
                "surname = $surname, " +
                "${patronymic?.let { "patronymic = $it, " } ?: ""}" +
                "gender = $gender, " +
                "age = $age, " +
                "dateOfBirth = $dateOfBirth" +
                "${inn?.let { ", inn = $it" } ?: ""}" +
                "${snils?.let { ", snils = $it" } ?: ""}"
    )
}

    fun main() {
    // С обязательными параметрами
    configurPerson(
        name = "Van",
        surname = "Darkholml",
        gender = "Male",
        dateOfBirth = "1980-01-04",
        age = 43
    )

    // Со всеми параметрами в произвольном порядке
    configurPerson(
        name = "Igor",
        inn = "1234567890",
        surname = "Nikolaev",
        dateOfBirth = "1960-01-30",
        patronymic = "Vladimirovich",
        gender = "FiveReasonMan",
        age = 63,
        snils = "111-111-111"
    )
}