package homework6

fun main() {
    val lion = Lion()
    val tiger = Tiger()
    val hippo = Hippo()
    val wolf = Wolf()
    val giraffe = Giraffe()
    val elephant = Elephant()
    val chimp = Chimp()
    val gorilla = Gorilla()

    val arrayOfAnimals: Array<Animal> = arrayOf(lion, tiger, hippo, wolf, giraffe, elephant, chimp, gorilla)
    val arrayOfFood: Array<String> =
        arrayOf("Banana", "Mango", "Apple", "Strawberry", "Meat", "Potato", "Tomato", "Cabbage")

    feedAnimals(arrayOfAnimals, arrayOfFood)
}

fun feedAnimals(animals: Array<Animal>, favouriteFood: Array<String>) {
    for (animal in animals) {
        for (food in favouriteFood) {
            animal.eat(food)
        }
    }
}

abstract class Animal {
    abstract val name: String
    abstract var height: Int
    abstract var weight: Double
    abstract val favouriteFood: Array<String>
    private var satiety = 0

    fun eat(food: String) {
        if (food in favouriteFood) {
            satiety++
        }
    }
}

class Lion : Animal() {
    override val name: String = "Лев"
    override var height: Int = 80
    override var weight: Double = 68.8
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Tiger : Animal() {
    override val name: String = "Тигр"
    override var height: Int = 70
    override var weight: Double = 57.5
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Hippo : Animal() {
    override val name: String = "Бегемот"
    override var height: Int = 170
    override var weight: Double = 100.9
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Wolf : Animal() {
    override val name: String = "Волк"
    override var height: Int = 60
    override var weight: Double = 34.8
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Giraffe : Animal() {
    override val name: String = "Жираф"
    override var height: Int = 250
    override var weight: Double = 77.1
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Elephant : Animal() {
    override val name: String = "Слон"
    override var height: Int = 2000
    override var weight: Double = 96.3
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Chimp : Animal() {
    override val name: String = "Шимпанзе"
    override var height: Int = 70
    override var weight: Double = 25.5
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}

class Gorilla : Animal() {
    override val name: String = "Горилла"
    override var height: Int = 1000
    override var weight: Double = 81.1
    override val favouriteFood = arrayOf("Banana", "Mango", "Apple", "Strawberry")
}