fun printStrings(vararg params: String) {
    var count = params.size

    println("Передано $count элемента")
    println(params.joinToString(separator = ";", postfix = ";"))
}

fun main() {
    printStrings("12","122","1234", "fpo")
}