class Companion {
    companion object {
        private var count = 0
        fun counter(){
            count += 1
            println("Вызван counter. Количество вызовов = $count")
        }
    }
}

fun main() {
    Companion.counter()
    Companion.counter()
}