package homework4

fun main() {
    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    for (i in 1..myArray.size) {
        var isSwapped = false
        for (j in 0 until myArray.size - i) {
            if (myArray[j + 1] < myArray[j]) {
                val swap = myArray[j + 1]
                myArray[j + 1] = myArray[j]
                myArray[j] = swap
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        print("$item ")
    }
}
