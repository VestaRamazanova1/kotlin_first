package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var captain = 0
    var team = 0
    for (i in myArray) {
        if (i % 2 == 0) {
            captain++
        } else {
            team++
        }
    }
    println("Капитан Джо получит монет: $captain")
    println("Команда получит монет: $team")
}