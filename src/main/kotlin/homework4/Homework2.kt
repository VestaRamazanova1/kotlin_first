package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    val total = marks.size
    var otlichniki = 0
    var horoshisty = 0
    var troechniki = 0
    var dvoechniki = 0
    for (i in marks) {
        when (i) {
            5 -> otlichniki++
            4 -> horoshisty++
            3 -> troechniki++
            2 -> dvoechniki++
        }
    }
    println("Отличников - ${otlichniki * 100F / total} %")
    println("Хорошистов - ${horoshisty * 100F / total} %")
    println("Троечников - ${troechniki * 100F / total} %")
    println("Двоечников - ${dvoechniki * 100F / total} %")
}