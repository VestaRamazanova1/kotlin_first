fun main() {
    val limonade: Int = 18500
    val pinacolada: Short = 200
    val wiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val el: Double = 0.666666667
    val author: String = "Что-то авторское!"
    println(limonade)
    println(pinacolada)
    println(wiskey)
    println(fresh)
    println(cola)
    println(el)
    println(author)
}