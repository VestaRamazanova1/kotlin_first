fun main() {
    val limonade: Int = 18500
    val pinacolada: Short = 200
    val wiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val el: Double = 0.666666667
    val author: String = "Что-то авторское!"
    println("Заказ - '$limonade мл лимонада' готов!")
    println("Заказ - '$pinacolada мл пина колады' готов!")
    println("Заказ - '$wiskey мл виски' готов!")
    println("Заказ - '$fresh капель фреша' готов!")
    println("Заказ - '$cola литра колы' готов!")
    println("Заказ - '$el литра эля' готов!")
    println("Заказ - '$author' готов!")
}