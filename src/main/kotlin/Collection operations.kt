import kotlin.math.pow
import kotlin.math.roundToInt

fun processList(list: List<Double?>): Double {
    return list
        .filterNotNull()
        .map {
            when {
                it.toInt() % 2 == 0 -> it * it
                else -> it / 2
            }
        }
        .filter { it <= 25 }
        .sortedDescending()
        .take(10)
        .sum()
        .roundTo(2)
}

fun Double.roundTo(numDecimalPlaces: Int): Double {
    val factor = 10.0.pow(numDecimalPlaces.toDouble())
    return (this * factor).roundToInt() / factor
}

fun main() {

    val myList = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    val result = processList(myList)
    println(result)
}