package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    println(deleteDuplicates(sourceList))
}

fun deleteDuplicates(inputList: List<Int>): List<Int> {
    return inputList.toSet().toList()
}
