package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    val totalVegetables = getVegetablesCount(userCart, vegetableSet)
    println("Количество овощей: $totalVegetables")
    val totalPrices = getTotalCost(userCart, prices, discountSet, discountValue)
    println("Стоимость всех товаров в корзине с учетом скидок: $totalPrices")
}

fun getVegetablesCount(cart: Map<String, Int>, vegetables: Set<String>): Int {
    var vegetablesSum = 0

    for ((product, quantity) in cart) {
        if (product in vegetables) {
            vegetablesSum += quantity
        }
    }
    return vegetablesSum
}

fun getTotalCost(cart: Map<String, Int>, cost: Map<String, Double>, cheapFood: Set<String>, discount: Double): Double {
    var totalCost = 0.0

    for ((product, quantity) in cart) {
        val price = cost[product] ?: 0.0
        val discountedPrice = if (cheapFood.contains(product)) {
            (1.0 - discount) * price
        } else {
            price
        }
        totalCost += quantity * discountedPrice
    }
    return totalCost
}
