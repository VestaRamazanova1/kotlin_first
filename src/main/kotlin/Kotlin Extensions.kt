val collection: MutableList<Int> = mutableListOf(1, 4, 9, 16, 25)

fun MutableList<Int>.squareElements() {
    for (i in indices) {
        this[i] = this[i] * this[i]
    }
}

fun main() {
    collection.squareElements()
    collection.forEach {
        println(it)
    }
}
