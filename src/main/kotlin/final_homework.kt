data class Coordinate(val x: Int, val y: Int)

class Ship(val size: Int) {
    var coordinates: List<Coordinate> = emptyList()

    fun placeShip(start: Coordinate, orientation: String) {
        val shipCoordinates = mutableListOf<Coordinate>()
        for (i in 0 until size) {
            val x = if (orientation == "H") start.x + i else start.x
            val y = if (orientation == "V") start.y + i else start.y
            shipCoordinates.add(Coordinate(x, y))
        }
        coordinates = shipCoordinates
    }

    fun isHit(coordinate: Coordinate): Boolean {
        return coordinates.contains(coordinate)
    }
}

class Player(val name: String) {
    val board: Array<Array<Char>> = Array(4) { Array(4) { '.' } }
    val ships: MutableList<Ship> = mutableListOf()

    fun placeShips() {
        val shipSizes = listOf(1, 1, 2)
        for (size in shipSizes) {
            println("$name, разместите $size-палубный корабль.")
            print("Введите начальные координаты (например, A1): ")
            val startInput = readLine()?.uppercase()
            print("Введите ориентацию (H для горизонтальной, V для вертикальной): ")
            val orientation = readLine()?.uppercase()
            if (startInput != null && orientation != null) {
                val start = Coordinate(startInput[0] - 'A', startInput.substring(1).toInt() - 1)
                val ship = Ship(size)
                ship.placeShip(start, orientation)
                if (isValidPlacement(ship)) {
                    ships.add(ship)
                    updateBoard(ship)
                    printBoard()
                } else {
                    println("Не возможно расположить. Попробуйте снова.")
                    placeShips()
                    break
                }
            }
        }
    }

    private fun isValidPlacement(ship: Ship): Boolean {
        return ship.coordinates.all { it.x in 0..3 && it.y in 0..3 && board[it.x][it.y] == '.' }
    }

    private fun updateBoard(ship: Ship) {
        for (coordinate in ship.coordinates) {
            board[coordinate.x][coordinate.y] = 'O'
        }
    }

    fun takeTurn(opponent: Player): Boolean {
        println("$name, Ваша очередь")
        print("Введите начальные координаты (например, A1): ")
        val targetInput = readLine()?.uppercase()
        if (targetInput != null) {
            val target = Coordinate(targetInput[0] - 'A', targetInput.substring(1).toInt() - 1)
            val hit = opponent.receiveAttack(target)
            if (hit) {
                println("Ранил!")
                return true
            } else {
                println("Мимо!")
            }
        }
        return false
    }

    private fun receiveAttack(coordinate: Coordinate): Boolean {
        for (ship in ships) {
            if (ship.isHit(coordinate)) {
                ship.coordinates = ship.coordinates.filter { it != coordinate }
                board[coordinate.x][coordinate.y] = 'X'
                if (ship.coordinates.isEmpty()) {
                    ships.remove(ship)
                    println("$name корабль убит")
                    if (ships.isEmpty()) {
                        println("У $name больше нет кораблей. $name победил!")
                        return true
                    }
                }
                return true
            }
        }
        board[coordinate.x][coordinate.y] = '*'
        return false
    }

    fun printBoard() {
        println("Доска игрока $name")
        for (row in board) {
            for (cell in row) {
                print("$cell ")
            }
            println()
        }
        println()
    }
}

fun main() {
    println("Добро пожаловать в морской бой")

    print("Введите имя 1го игрока: ")
    val player1Name = readLine() ?: "Игрок 1"
    val player1 = Player(player1Name)
    player1.placeShips()

    print("Введите имя 2го игрока: ")
    val player2Name = readLine() ?: "Игрок 2"
    val player2 = Player(player2Name)
    player2.placeShips()

    var currentPlayer = player1
    var opponent = player2

    while (true) {
        currentPlayer.printBoard()
        if (currentPlayer.takeTurn(opponent)) {
            continue
        }

        // Switch players
        val temp = currentPlayer
        currentPlayer = opponent
        opponent = temp
    }
}