import java.util.*
import java.io.File
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.PDPageContentStream
import com.github.javafaker.Faker
import java.text.SimpleDateFormat
import org.apache.pdfbox.pdmodel.font.PDType0Font
import com.google.gson.GsonBuilder

// Модели данных
data class Persona(
    val firstName: String,
    val lastName: String,
    val middleName: String,
    val age: Int,
    val gender: Char,
    val birthDate: String,
    val birthPlace: String,
    val address: Address
)

data class Address(
    val postalCode: String,
    val country: String,
    val region: String,
    val city: String,
    val street: String,
    val house: String,
    val apartment: String
)


val maleNames = listOf("Николай", "Тарас", "Андрей", "Евгений", "Антон")
val femaleNames = listOf("Мария", "Анна", "Елена", "Дарья", "Ирина")

val maleLastNames = listOf("Ляхов", "Щукин", "Сидоров", "Кузнецов", "Смирнов")
val femaleLastNames = listOf("Ляхова", "Щукина", "Сидорова", "Кузнецова", "Смирнова")

val malePatronymics = listOf("Александрович", "Иванович", "Михаилович", "Дмитриевич", "Максимович")
val femalePatronymics = listOf("Александровна", "Ивановна", "Михайловна", "Дмитриевна", "Максимовна")

val dateFormatter = SimpleDateFormat("dd-MM-yyyy")

fun generateRandomPerson(faker: Faker): Persona {
    val gender = if (faker.bool().bool()) 'М' else 'Ж'
    val firstName = if (gender == 'М') maleNames.random() else femaleNames.random()
    val lastName = if (gender == 'М') maleLastNames.random() else femaleLastNames.random()
    val middleName = if (gender == 'М') malePatronymics.random() else femalePatronymics.random()
    val age = faker.number().numberBetween(18, 100)
    val birthDate = dateFormatter.format(faker.date().birthday(age, age))
    val birthPlace = faker.address().city()
    val address = Address(
        postalCode = faker.address().zipCode(),
        country = "Россия",
        region = faker.address().state(),
        city = faker.address().cityName(),
        street = faker.address().streetName(),
        house = faker.address().buildingNumber(),
        apartment = faker.number().numberBetween(10, 900).toString()
    )

    return Persona(firstName, lastName, middleName, age, gender, birthDate, birthPlace, address)
}

fun main() {
    var n: Int? = null

    while (n == null || n !in 0..30) {
        println("Введите число n в интервале [0;30]: ")
        val input = readLine() ?: ""
        n = input.toIntOrNull()

        if (n == null || n !in 0..30) {
            println("Введено некорректное значение. Пожалуйста, введите число в интервале от 0 до 30.")
        }
    }

    val faker = Faker(Locale("ru"))
    val persons = (1..n).map { generateRandomPerson(faker) }

    saveToJson(persons)
    saveToPdf(persons)
}

fun saveToJson(persons: List<Persona>) {
    val gson = GsonBuilder().setPrettyPrinting().create()
    val json = gson.toJson(persons)
    try {
        File("persons.json").writeText(json, Charsets.UTF_8)
        println("Файл создан. Путь: ${File("persons.json").absolutePath}")
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun saveToPdf(persons: List<Persona>) {
    val document = PDDocument()
    val landscapePageSize = PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth())
    val page = PDPage(landscapePageSize)
    document.addPage(page)
    val contentStream = PDPageContentStream(document, page, PDPageContentStream.AppendMode.OVERWRITE, true, true)

    val font = PDType0Font.load(
        document,
        PDType0Font::class.java.getResourceAsStream("/org/apache/pdfbox/resources/ttf/LiberationSans-Regular.ttf")
    )
    val fontSize = 8f
    val margin = 15f
    val padding = 0.5f
    var yPosition = landscapePageSize.getHeight() - margin

    // Вычисление максимальной ширины столбца
    val columnWidths = mutableListOf<Float>()
    val headers = listOf(
        "Имя",
        "Фамилия",
        "Отчество",
        "Возраст",
        "Пол",
        "Дата рождения",
        "Место рождения",
        "Индекс",
        "Страна",
        "Область",
        "Город",
        "Улица",
        "Дом",
        "Квартира"
    )
    headers.forEach { header ->
        columnWidths.add(font.getStringWidth(header) / 1000 * fontSize + 2 * padding)
    }
    persons.forEach { person ->
        listOf(
            person.firstName,
            person.lastName,
            person.middleName,
            person.age.toString(),
            person.gender.toString(),
            person.birthDate, person.birthPlace,
            person.address.postalCode,
            person.address.country,
            person.address.region,
            person.address.city,
            person.address.street,
            person.address.house,
            person.address.apartment
        )
            .forEachIndexed { index, data ->
                val width = font.getStringWidth(data) / 1000 * fontSize + 2 * padding
                if (columnWidths[index] < width) {
                    columnWidths[index] = width
                }
            }
    }

    // Отрисовка заголовков и границ
    var xPosition = margin
    headers.forEachIndexed { index, header ->
        addText(contentStream, header, font, fontSize, xPosition + padding, yPosition - fontSize - padding)
        drawCellBorder(
            contentStream,
            xPosition,
            yPosition - fontSize - 2 * padding - 2,
            columnWidths[index],
            fontSize + 2 * padding + 2
        )
        xPosition += columnWidths[index] + padding
    }
    yPosition -= fontSize + 2 * padding + 2

    // Отрисовка данных и границ
    persons.forEach { person ->
        xPosition = margin
        listOf(
            person.firstName,
            person.lastName,
            person.middleName,
            person.age.toString(),
            person.gender.toString(),
            person.birthDate,
            person.birthPlace,
            person.address.postalCode,
            person.address.country,
            person.address.region,
            person.address.city,
            person.address.street,
            person.address.house,
            person.address.apartment
        )
            .forEachIndexed { index, data ->
                addText(contentStream, data, font, fontSize, xPosition + padding, yPosition - fontSize - padding)
                drawCellBorder(
                    contentStream,
                    xPosition,
                    yPosition - fontSize - 2 * padding - 2,
                    columnWidths[index],
                    fontSize + 2 * padding + 2
                )
                xPosition += columnWidths[index] + padding
            }
        yPosition -= fontSize + 2 * padding + 2
    }

    contentStream.close()
    val pdfFile = File("persons.pdf")
    document.save(pdfFile)
    println("Файл создан. Путь: ${pdfFile.absolutePath}")
    document.close()
}

fun drawCellBorder(contentStream: PDPageContentStream, x: Float, y: Float, width: Float, height: Float) {
    contentStream.addRect(x, y, width, height)
    contentStream.stroke()
}

fun addText(
    contentStream: PDPageContentStream,
    text: String,
    font: PDType0Font,
    fontSize: Float,
    xPosition: Float,
    yPosition: Float
) {
    contentStream.beginText()
    contentStream.setFont(font, fontSize)
    contentStream.newLineAtOffset(xPosition, yPosition)
    contentStream.showText(text)
    contentStream.endText()
}