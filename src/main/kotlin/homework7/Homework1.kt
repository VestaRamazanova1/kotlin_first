package homework7

import java.lang.Exception

fun main() {
    val user = createUser("Vesta", "Ramazanova", "Ramazanova")
    println(user)
}

data class User(val login: String, val password: String)

fun createUser(login: String, password: String, confirmation: String): User {
    if (login.length > 20) {
        throw WrongLoginException("В логине не должно быть более 20 символов")
    }

    if (password.length < 10) {
        throw WrongPasswordException("В пароле не должно быть менее 10 символов")
    }

    if (password != confirmation) {
        throw WrongPasswordException("Пароль и подтверждение не совпадают")
    }
    return User(login, password)
}

class WrongLoginException(message: String) : Exception(message)

class WrongPasswordException(message: String) : Exception(message)
