data class Person(
    val name: String,
    var surname: String,
    val gender: String,
    val dateOfBirth: String,
    var age: Int,
    val patronymic: String? = null,
    var inn: String? = null,
    var snils: String? = null,
)

fun configurePerson(
    name: String,
    surname: String,
    gender: String,
    dateOfBirth: String,
    age: Int,
    patronymic: String? = null,
    inn: String? = null,
    snils: String? = null
): Person {
    return Person(
        name = name,
        surname = surname,
        gender = gender,
        dateOfBirth = dateOfBirth,
        age = age,
        patronymic = patronymic,
        inn = inn,
        snils = snils
    )
}

fun main() {
    // Создание объекта Person с необязательными параметрами
    println(
        configurePerson(
            name = "Van",
            surname = "Darkholm",
            gender = "Male",
            dateOfBirth = "1980-01-04",
            age = 43
        )
    )
}


