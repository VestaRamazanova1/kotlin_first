import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.After
import org.junit.Before
import java.io.ByteArrayOutputStream
import java.io.PrintStream


@RunWith(Parameterized::class)
class CalculatorTest(
    private val method: String,
    private val a: Double,
    private val b: Double,
    private val expected: String
) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: Test with method={0}, a={1}, b={2}, expected={3}")
        fun data(): Collection<Array<Any>> {
            return listOf(
                arrayOf("+", 2.0, 3.0, "5.0"),
                arrayOf("-", 2.0, 3.0, "-1.0"),
                arrayOf("*", 2.0, 3.0, "6.0"),
                arrayOf("/", 6.0, 3.0, "2.0"),
                arrayOf("/", 1.0, 0.0, "Infinity"), // Деление на ноль
                arrayOf("*", 0.0, 5.0, "0.0"), // Умножение на ноль
                arrayOf("/", 10.0, 3.0, (10.0 / 3.0).toString()), // Деление с плавающей точкой
                arrayOf("-", -5.0, -7.0, "2.0"), // Отрицательные числа
                arrayOf("+", Double.MAX_VALUE, Double.MAX_VALUE, "Infinity"), // Граничные значения для очень больших чисел
                arrayOf("*", Double.MIN_VALUE, Double.MIN_VALUE, "0.0") // Граничные значения для очень маленьких чисел
            )
        }
    }
    private val outputStreamCaptor = ByteArrayOutputStream()

    @Before
    fun setUp() {
        System.setOut(PrintStream(outputStreamCaptor))
    }

    @Test
    fun testCalculator() {
        calculator(getCalculationMethod(method), a, b)

        // Сравнение ожидаемой строки результата с фактическим выводом
        assertEquals(expected.trim(), outputStreamCaptor.toString().trim())
    }

    @After
    fun tearDown() {
        System.setOut(System.out)
        outputStreamCaptor.reset()
    }
}


